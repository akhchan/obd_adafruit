//Main Code for Arduino OBD Logger
#include <Adafruit_GFX.h>
#include <gfxfont.h>
#include <OBD2UART.h>
#include <Adafruit_ILI9340.h>
#include <SD.h>

#define PRINT_TRIAL 0

#define DISPLAY_0 0
#define DISPLAY_1 1
#define DISPLAY_2 2
#define DISPLAY_3 3
#define DISPLAY_4 4
#define DISPLAY_5 5
//Pins definition for the LCD Display 
#define _sclk 13
#define _miso 12
#define _mosi 11
#define _cs 10
#define _dc 9
#define _rst 8

#define maxRPM 8000
#define maxPercent 100

int SavetoSD(int data[], char* fileName);                     //Saves to SD, returns 1 if success and 0 if failure
int readDataSD(byte PID, int& data);
void fillGraph(int data, int maxVal);                              //Write to LCD given data, and max value
void historyGraph(int data,int maxVal,int graphPosition,int color);

COBD obd;
File DataLog;                                                         // SD File Type
char fileName[13] = "TRIP0000.csv";
int buttonState = 0;                                                  //Button Globals
int lastButtonState = 0;
int state = 0;                                                            //Other
unsigned long t;
const int n = 6;                                                      //OBD constants, n = how many pieces of data we are requesting
const byte PIDs[n] = {PID_RPM, PID_SPEED, PID_ENGINE_LOAD, PID_ENGINE_FUEL_RATE,PID_COOLANT_TEMP,PID_RUNTIME};                         //current PIBs are test examples
Adafruit_ILI9340 tft = Adafruit_ILI9340(_cs, _dc, _rst);              // Use hardware SPI
int count = 0;                                                        //Used for test array
long graphY = 0;
long lastY = 0;
bool screenreset = true;
int readCount = 0;
int data[n];
unsigned long filePos = 0;
int graphPosition;

//=======================================================================================================================
void setup() {
  
  SD.begin(7);                                                       //SD Initialized
  pinMode(7, OUTPUT);
  
  pinMode(2, INPUT);                                                 //Button Initialized
  obd.begin();                                                     //OBD Initialized
  do {
    Serial.println("OBD initializing...");
  } while(!obd.init());
  
  state = DISPLAY_0;                                                 //Set default state
  tft.begin();  
  
  tft.fillScreen(ILI9340_BLACK);                                     //Set Screen to Black
  tft.setRotation(45);
  tft.setTextColor(ILI9340_WHITE,ILI9340_BLACK);  tft.setTextSize(3);     //Set text to White, Size 1 
  // read a file telling you what trip number you are on and create a new file called "TRIP(tripNum).csv"
  int tripVal = 0;
  DataLog = SD.open("prevtrip.txt");
  if(DataLog){
    if (DataLog.available()){
      tripVal = int(DataLog.read() - '0');
      while (DataLog.available()){
        tripVal = tripVal*10 + int(DataLog.read() - '0');
      }
      DataLog.close();
      SD.remove("prevtrip.txt");
    }
    else{
      tripVal = 1;
    }
  }
  
  if(tripVal >= 10000){      //possible to set storage error instead of looping back to trip 1
    tripVal = 1;
  }
  
  if(1 <= tripVal && tripVal < 10){ 
    char tripNum = tripVal + '0';
    fileName[7] = tripNum;
  }
  else if(10 <= tripVal && tripVal < 100){ 
    char tripNum = floor(tripVal/10) + '0';
    fileName[6] = tripNum;
    tripNum = tripVal%10 + '0';
    fileName[7] = tripNum;
  }
  else if(100 <= tripVal && tripVal < 1000){ 
    char tripNum = floor(tripVal/100) + '0';
    fileName[5] = tripNum;
    tripNum = floor((tripVal%100)/10) + '0';
    fileName[6] = tripNum;
    tripNum = tripVal%10 + '0';
    fileName[7] = tripNum;
  }
  else if(1000 <= tripVal && tripVal < 10000){ 
    char tripNum = floor(tripVal/1000) + '0';
    fileName[4] = tripNum;
    tripNum = floor((tripVal%1000)/100) + '0';
    fileName[5] = tripNum;
    tripNum = floor((tripVal%100)/10) + '0';
    fileName[6] = tripNum;
    tripNum = tripVal%10 + '0';
    fileName[7] = tripNum;
  }
  DataLog = SD.open("prevtrip.txt", FILE_WRITE); // replace value with that of the current trip num
  if (DataLog){
    DataLog.print(tripVal + 1);
    DataLog.close();
  }
  
  }
//=======================================================================================================================
void loop() {  
  buttonState = digitalRead(2);             //Button checking code
  if (buttonState == HIGH && buttonState != lastButtonState){
    if (state != DISPLAY_5)
      state++;
    else
      state -= 5;
    screenreset = true;
  }
  lastButtonState = buttonState;
  //Reset screen to black when entering a new state
  if(screenreset){
    tft.fillScreen(ILI9340_BLACK);
    screenreset = false;
    graphPosition = 0;
  }

  if(graphPosition == 320){
    graphPosition = 0;
  }
  
if(!PRINT_TRIAL){
  if (readCount != state)
    obd.readPID(PIDs[readCount], data[readCount]);
}
else{
  //readDataSD(data);
}
  switch(state) {
    case DISPLAY_0: //PID_RPM
      
      if(!PRINT_TRIAL){
              obd.readPID(PIDs[state], data[state]);
      }
      else{
              //readDataSD(PIDs[state], data[state]);
      }
      
      tft.setCursor(5, 5); 
      tft.setTextSize(3);
      tft.print("RPM : ");
      tft.print(data[0]);
      
      historyGraph(data[0], 7000 ,graphPosition,ILI9340_GREEN);
       
       break;
    case DISPLAY_1:           //PID_SPEED
    
      if(!PRINT_TRIAL){
              obd.readPID(PIDs[state], data[state]);
      }
      else{
              //readDataSD(PIDs[state], data[state]);
      }

      tft.setCursor(5, 5); 
      tft.setTextSize(3);
      tft.print("Speed : ");
      tft.print(data[1]);
      tft.print("km/h");
      
      if(data[1] < 10){
      tft.fillRect(0,0,100,140,ILI9340_BLACK);  //NEED TO FIX!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
      }                                           //this one doesnt work at all because i dont know where the 0 ends up
      
      historyGraph(data[1], 200 ,graphPosition,ILI9340_RED);

      break;
    case DISPLAY_2:           //PID_ENGINE_LOAD

      if(!PRINT_TRIAL){
              obd.readPID(PIDs[state], data[state]);
      }
      else{
              //readDataSD(PIDs[state], data[state]);
      }
      
      tft.setCursor(5, 5); 
      tft.setTextSize(3);
      tft.print("Engine Load : ");
      tft.setCursor(40,100 ); 
      tft.setTextSize(7);

      if(data[2] < 10){
      tft.fillRect(165,100,38,50,ILI9340_BLACK);  //NEED TO FIX
      }
      
      tft.print(data[2]);
      tft.setTextSize(4);
      tft.setCursor(200,100); 
      tft.print(" %");

      break;
    case DISPLAY_3:     //PID_ENGINE_FUEL_RATE
    
      if(!PRINT_TRIAL){
              obd.readPID(PIDs[state], data[state]);
      }
      else{
              //readDataSD(PIDs[state], data[state]);
      }

      tft.setCursor(5, 5); 
      tft.setTextSize(3);
      tft.print("Fuel Consumption : ");
      tft.setCursor(40,100 ); 
      tft.setTextSize(7);
      
      tft.print(data[2]);
      tft.setTextSize(4);
      tft.setCursor(200,100); 
      tft.print(" L/h");

      break;
    case DISPLAY_4:     //PID_COOLANT_TEMP
    
      if(!PRINT_TRIAL){
              obd.readPID(PIDs[state], data[state]);
      }
      else{
              //readDataSD(PIDs[state], data[state]);
      }
      
      tft.setCursor(5, 5); 
      tft.setTextSize(3);
      tft.print("Coolant Temp : ");
      tft.setCursor(40,100 ); 
      tft.setTextSize(7);

      tft.print(data[4]);
      tft.setTextSize(4);
      tft.setCursor(200,100); 
      tft.print(" C");
      
      break;
    case DISPLAY_5:  //PID_RUNTIME
    
      if(!PRINT_TRIAL){
              obd.readPID(PIDs[state], data[state]);
      }
      else{
             //readDataSD(PIDs[state], data[state]);
      }
      
      tft.setCursor(5, 5); 
      tft.setTextSize(3);
      tft.print("Run Time : ");
      tft.setCursor(40,100 ); 
      tft.setTextSize(7);

      if (data[5] % 60 == 0)
        tft.print(data[5]/60);
      tft.setTextSize(4);
      tft.setCursor(200,100); 
      tft.print(" min");

      break;
  }

  if (readCount == n){
    if(SavetoSD(data, fileName)){
      //Serial.println("save successful");
    }
    else{
      //Serial.println("save failed");
    }
  }

  graphPosition++;
  readCount++;
  if (readCount == n+1)
    readCount = 0;
}
//=======================================================================================================================
int SavetoSD(int data[], char* fileName){
  t = millis();                              //Write to SD
  
  DataLog = SD.open(fileName, FILE_WRITE);
  
  if (DataLog){
    DataLog.print(t);                               //Write current time since program started (sec)
    for(int i = 0; i < n; i++){
      DataLog.print(",");
      DataLog.print(data[i]);
    }
    DataLog.print("\n");
  }
  else{
    return 0;
  }
  DataLog.close();
  return 1;
}

int readDataSD(int data[]){
  
  int ValLoc = 0;
  int DataLoc = 0;
  char val[20];
  int i = 0;
  DataLog = SD.open("TRIP0001.csv");
  if(DataLog){
    DataLog.seek(filePos);
    while (DataLog.available()){
      val[i] = DataLog.read();
      if(val[i] == ','){
        val[i] = '\0';
        if (ValLoc >= 1){
          data[DataLoc] = atoi(val + ValLoc);
          ValLoc = i+1;
          DataLoc++;
        }
        if (ValLoc == 0)
          ValLoc = i+1;
      }
      if(val[i] == '\n'){
        val[i] = '\0';
        if (ValLoc >= 1)
          data[DataLoc] = atoi(val + ValLoc);
        filePos = DataLog.position();
        DataLog.close();
        return 1;
      }
      i++;
    }
  }
}

void fillGraph(int data, int maxVal){
  
  graphY =  (long) data * 210 / maxVal ;
  tft.drawFastHLine(0,240-lastY,320, ILI9340_BLACK);       //Reset bottom part
  tft.drawFastHLine(0,240-graphY,320, ILI9340_GREEN);
      
  lastY = graphY;
}

void historyGraph(int data,int maxVal,int graphPosition,int color){
   graphY =  (long) data * 210 / maxVal ;
  
   tft.fillRect(graphPosition, 33, 20, 210,ILI9340_BLACK); 
   tft.drawLine(graphPosition - 1, 240-lastY, graphPosition, 240-graphY, color);
   
   lastY = graphY; 
   graphPosition++; 
}

